<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert( [ [ 
            'user_id'=> '1',
            'title' => 'Shir',
            'description'=>'bjhbcjhbdcjhwbdj',
            'url' => 'Shir@gmail.com',
            'created_at' => date('Y-m-d G:i:s')
], ]);

DB::table('profiles')->insert( [ [ 
    'user_id'=> '2',
    'title' => 'amir',
    'description'=>'bjhbcjhbdcjhwbdj',
    'url' => 'amir@gmail.com',
    'created_at' => date('Y-m-d G:i:s')
], ]);

    }
}
