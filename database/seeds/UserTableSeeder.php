<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert( [ [ 
            'id'=>'1',
            'name' => 'Shir',
            'username'=>'Shir123',
            'email' => 'Shir@gmail.com',
            'password' =>Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s')
], ]);

DB::table('users')->insert( [ [ 
    'id'=>'2',
    'name' => 'test1',
    'username'=>'tasty1234',
    'email' => 'testy@gmail.com',
    'password' =>Hash::make('12345678'),
    'created_at' => date('Y-m-d G:i:s')
], ]);
    }
}
