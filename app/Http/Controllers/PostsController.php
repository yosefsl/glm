<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
     
        return view('posts.create');
    } 
    
    public function store()
    {
        $data= request()->validate([
            'caption'=>'requierd',
            'image'=>'requierd|image',
        ]);

         $ImagPath = request('image')->store('uploads','public');
           
             auth()->user()->posts()->create([
            'caption'=>$data['caption'],
            'image'=>$imagePath,
        ]);



        return view('/profile/'.auth()->user()->id);

    } 

}
